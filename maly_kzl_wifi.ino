#include <avr/wdt.h>
#include <FlexiTimer2.h>

#define ARRAY_SIZE 89
#define BOARDCOUNT 1

//Bit portu B ktory jest zegarem (3 = Pin 11, 2 = Pin 10)
#define CLKBIT 2


#include "letter.h"
#include "letterarray.h"

#include "bytearraywriter.h"
#include "paddedbytearraywriter.h"
#include "circularbytearraywriter.h"
#include "letterarraywriter.h"
#include "boardconfigurator.h"


const char server_ip[] = "999.999.999.999";
const char server_port[] = "0000";
const char ssid[] = "SSID";
const char pass[] = "Pass";
const char name[] = "Name";

#define INSIZE 4
#define BAUD_RATE 115200

uint8_t inputBuf[INSIZE] = {0,0,0,0}; //Wifi input buffer
uint8_t commandBuf[3] = {0,0,0};
#define espWifi Serial

char debugBuf[400] = {0};
char *debugPtr = debugBuf;

bool wifiOperating = false;

enum ConStateEnum{Resetting, NetConnect, ServConnect, Handshake, Working};
ConStateEnum conState = Resetting;

String toBeSent = "";

void resetWifi()
{
  //Serial.write("Reset wifi!\n");

  espWifi.end();
  espWifi.begin(BAUD_RATE);
  conState = Resetting;
  wifiOperating = false;
  espWifi.write("AT+RST\r\n");
}

void sendOverWifi(String str)
{
  toBeSent = str;

  espWifi.write("AT+CIPSEND=");
  espWifi.write(String(str.length()+2).c_str());
  espWifi.write("\r\n");
}

void rewindConnection()
{
  switch(conState)
  {
    case Resetting:
    case NetConnect:
      conState = Resetting;
      break;
    case ServConnect:
      conState = NetConnect;
      break;
    case Handshake:
      conState = ServConnect;
      break;
    case Working:
      conState = Handshake;
      break;
  }
}

void pushConnection()
{
  switch(conState)
  {
    case Resetting:
      if(memmem(inputBuf, INSIZE,  "dy", 2) != NULL)
      {
        wifiOperating = true;
      }  

      if(!wifiOperating)
        break;

      //espWifi.write("AT+CWLAP\r\n");
      conState = NetConnect;
      //wifiOperating= false;
      
      espWifi.write("AT+CWJAP=\"");
      espWifi.write(ssid);
      espWifi.write("\",\"");
      espWifi.write(pass);
      espWifi.write("\"\r\n");

     
      break;
    case NetConnect:
      
      if(!wifiOperating)
        break;
      if(memmem(inputBuf, INSIZE, "OK", 2) == NULL) break;

      /*espWifi.write("AT+CIPMUX=1\r\n");
      conState = Cipmux;
      break;

    case Cipmux:
      if(strstr(inputBuf, "OK") == NULL) break;*/
      /*espWifi.write("AT+CIPSTART=1,\"TCP\",\"");
      espWifi.write(server_ip);
      espWifi.write("\",");
      espWifi.write(server_port);
      espWifi.write("\r\n");*/
      espWifi.write("AT+CIPSTART=\"TCP\",\"");
      espWifi.write(server_ip);
      espWifi.write("\",");
      espWifi.write(server_port);
      espWifi.write("\r\n");
      conState = ServConnect;
      //digitalWrite(13, HIGH); 

      break;

    case ServConnect:
      if(memmem(inputBuf, INSIZE, "OK", 2) == NULL) break;


      //7 = NAME: + \\r + \\n
      //espWifi.write("AT+CIPSEND=1,");
      espWifi.write("AT+CIPSEND=");
      espWifi.write(String(strlen(name)+7).c_str());
      espWifi.write("\r\n");
      conState = Handshake;
      break;

    case Handshake:
      if(memmem(inputBuf, INSIZE, ">", 1) == NULL) break;
      espWifi.write("NAME:");
      espWifi.write(name);
      espWifi.write("\r\n\r\n");
      conState = Working;
      break;

    case Working:
      //Keep alive
      if(memmem(inputBuf, INSIZE, ">" ,1) == NULL) break;
      if(toBeSent == "") break;

      espWifi.write(toBeSent.c_str());
      espWifi.write("\r\n\r\n");
      toBeSent = "";
      /*else if(sendKeepAlive)
      {
        espWifi.write("K\r\n\r\n");
        sendKeepAlive = false;
      }
      else if(sendRR)
      {
        espWifi.write("RR\r\n\r\n");
        sendRR = false; 
      }*/

      break;
  }
}


uint8_t letterToIndex(char let)
{
  if(let == ' ')
    return 0;
  if(let == '-')
    return 37;
  if(let == '.')
    return 36;
  if (let >= 'A' && let <= 'Z')
    return let - 'A' + 1;
  if (let >= '0' && let <= '9')
    return let - '0' + 1;
  else switch(let)
  {
    case 'a' : return 27;
    case 'c' : return 28;
    case 'e' : return 29;
    case 'l' : return 30;
    case 'n' : return 31;
    case 'o' : return 32;
    case 's' : return 33;
    case 'z' : return 35; //ż (zet)
    case '+' : return 34; //Ź (ziet)
    default: return 36;
  }
}


LetterArray lArray[5];
BoardConfigurator configurator;


void setModuleTo(int address, int group, int position)
{
  lArray[group].setLetter(address, position, (address == 0));

}

void setText(int address_off, int group, char* text)
{
  int len = strlen(text);
  for(int i = 0; i < len; ++i)
  {
    lArray[group].setLetter(i+address_off+1, letterToIndex(text[i]));
  }
  configurator.rewind();
}

// the setup routine runs once when you press reset:
void setup() {               
  //Zegar

  pinMode(8, OUTPUT); //Zegar

  pinMode(10, OUTPUT); 
  
  pinMode(11, OUTPUT); //Rot - B.3
  pinMode(2, OUTPUT);  //Dane - LET 1 - D.2 
  pinMode(3, OUTPUT);  //Dane - LET 1 - D.2 

  pinMode(4, OUTPUT);  //Dane - LET 1 - D.2 

  pinMode(5, OUTPUT);  //Dane - LET 1 - D.2 

  pinMode(6, OUTPUT);  //Dane - LET 1 - D.2 
  pinMode(7, OUTPUT); 
  pinMode(12, OUTPUT);  //Dane - LET 1 - D.2 

  
  pinMode(9, OUTPUT);  //Reset
  digitalWrite(9, HIGH);  
  digitalWrite(2, HIGH); 
  digitalWrite(3, HIGH); 
  digitalWrite(4, HIGH); 
  digitalWrite(5, HIGH); 
  digitalWrite(6, HIGH); 
  digitalWrite(12, HIGH);

  pinMode(13, OUTPUT);
  digitalWrite(13, LOW); 

      
  //510 ~= 980Hz 
  //Timer1.initialize(510);         
  // initialize timer1, and set a 1/2 second period              
  // setup pwm on pin 9, 50% duty cycle
  //TOP = 100
  //250 = 2KHz
  //Timer1.initialize(300);
  //Timer1.attachInterrupt(callback);  // attaches callback() as a timer overflow interrupt  

  configurator.addArray(&lArray[0]);
  

  FlexiTimer2::set(10, 1./10000., callback); //1KHz
  FlexiTimer2::start();

  Serial.begin(BAUD_RATE);
  //resetWifi();
  Serial.print("Hello!");

  //esp.begin(115200);

  wdt_enable(WDTO_4S);
}




void abitWrite(int, int, bool bit)
{
  Serial.print(bit ? "A" : "a");
}
void bbitWrite(int, int, bool bit)
{
  Serial.print(bit ? "B" : "b");
}
void rbitWrite(int, int, bool bit)
{
  Serial.print(bit ? "R" : "r");
}


void callback()
{
  bool hi = !bitRead(PORTB, CLKBIT);
  
  if(!hi && configurator.hasMore())
  {    
    uint8_t pB = PORTB;
    
    //bitWrite(pD, 3, configurator.getNextBit(1));
    bitWrite(pB, 3, configurator.getNextBit(0));
    

    //Rot_buf
    bitWrite(pB, 4, configurator.getNextBit(-1));

    PORTB = pB;
    
    bitWrite(PORTB, CLKBIT, 0);
    //bitWrite(PORTD, 7, 1);
  }

  if(!configurator.hasMore() || hi)
  {
    //Tylko zegar trzeba ustawic
    uint8_t reg = PORTB;
    bitWrite(reg, CLKBIT, hi); 

    PORTB = reg;
  }
  
}

char serbuf[100];
char txtbuf[100];

char *serptr = serbuf;

int off = 0;
bool sendKeepAlive = false;

void parseCommandBuffer()
{
  if(commandBuf[0] == '!' && commandBuf[1] == '!' && commandBuf[2] == '!')
  {
    configurator.rewind();
    return;
  }
  if(commandBuf[0] == 'C' && commandBuf[1] == 'H' && commandBuf[2] == 'K')
  {
    sendKeepAlive = true;
    return;
  } 
  else
  {
    uint8_t tab_id = commandBuf[0];
    uint8_t mod_id = commandBuf[1];
    uint8_t md_pos = commandBuf[2];

    if(tab_id < BOARDCOUNT &&
      mod_id <= 88 && //TODO: Hardcode
      md_pos < 40)
      setModuleTo(mod_id, tab_id, md_pos);
  }
}

void parseSerial()
{
  char cmd,c;
  int a,p,g;
  sscanf(serbuf, "%c %d %d %d", &cmd, &a, &g, &p);

  if(cmd == 'a')
  {
    String out = "Settting module ";
    out += a;
    out += ".";
    out += g;
    out += " to position ";
    out += p;
    out += "\n";
    Serial.print(out);
   
    setModuleTo(a,g, p);
  }
  if(cmd == 'b')
  {
    sscanf(serbuf, "%c %d %d %c", &cmd, &a, &g, &c);
    String out = "Settting module ";
    out += a;
    out += ".";
    out += g;
    out += " to position ";
    out += c;
    out += "\n";
    Serial.print(letterToIndex(c));
   
    setModuleTo(a,g, letterToIndex(c));
  }
  else if(cmd == 's')
  {
    Serial.print("Setting text starting at ");
    Serial.print(a);
    Serial.print(" : ");
    sscanf(serbuf, "%c %d %d %[^\r\n]", &cmd, &a, &g, txtbuf);
    Serial.print(g);
    Serial.print(" : ");

    Serial.print(txtbuf);
    setText(a, g,  txtbuf);
  }
  else if(cmd == 'r')
  {
    Serial.print("Go!");
    configurator.rewind();    
  }
  else if(cmd == 'e')
  {
    Serial.print("Forwarding to ESP...");
    Serial.print(&serbuf[2]);
    Serial.print("\r\n");

//    esp.print(&serbuf[2]);
//    esp.print("\r\n");

  }

  serptr = serbuf;
}

long int cnt = 0;
  int a = 1;
  int p = 1;

bool dataSizeParsed = false;
uint16_t dataSize = 0;
uint16_t bytesRead = 0;


enum BufferState{Closed, Preparing, ReadingCommand};
volatile BufferState bufState = Closed;

unsigned long lastWifiActivity = 0L;


void loop() {


  if(Serial.available() > 0) {
    // read the incoming byte:
    *serptr = Serial.read();

    if(*serptr == '\n')
    {
      *serptr = 0;
      parseSerial();
    }
    else
      serptr++;
  }

  /*
  if(espWifi.available() > 0)
  {

     digitalWrite(13, HIGH);

    char wifiCh = espWifi.read();


    lastWifiActivity = millis();

    inputBuf[0] = inputBuf[1];
    inputBuf[1] = inputBuf[2];
    inputBuf[2] = wifiCh;

    if(bufState == Closed && (inputBuf[2] == '>' || inputBuf[2] == '\n' || inputBuf[2] == '\r'))
    {
        pushConnection();
    }
    else if(strcmp("IPD", (char*)inputBuf) == 0)
    {
      dataSizeParsed = false;  
      dataSize = 0;     
      bytesRead = 0;
      bufState = Preparing;
    }
    else if(bufState == Preparing)
    {
      if(!dataSizeParsed && isdigit(inputBuf[2])){
        dataSize *= 10;
        dataSize += (inputBuf[2]-'0');
      }
      else if(inputBuf[2]==':')
      {
        dataSizeParsed = true;
        bufState = ReadingCommand;
        digitalWrite(13, HIGH);
      }
    }
    else if(bufState == ReadingCommand)
    {
      bytesRead++;
      commandBuf[0] = commandBuf[1];
      commandBuf[1] = commandBuf[2];
      commandBuf[2] = inputBuf[2];

      if(bytesRead % 3 == 0)
      {
        parseCommandBuffer();
      }
      if(bytesRead == dataSize)
      {
        if(sendKeepAlive)
        {
          sendOverWifi("KKK");
          sendKeepAlive = false;
        }  
        else
        {
          String txt = "Parsed ";
          txt += dataSize;

          sendOverWifi(txt);
        }
        bufState = Closed;
      }
    }
  }

  unsigned long tm = millis();

  if(tm < lastWifiActivity) //overflow
    lastWifiActivity = tm;

  else if(tm - lastWifiActivity > 60*1000L) //Minuta
  {
    lastWifiActivity = tm;
    resetWifi();
  }

  wdt_reset();*/
}
