#pragma once

#include "debug.h"
#include "paddedbytearraywriter.h"
#include "circularbytearraywriter.h"
#include "letterarraywriter.h"

class BoardConfigurator{

  class RotationWriter : public PaddedByteArrayWriter{
  public:
    RotationWriter() : PaddedByteArrayWriter(20){
      setRotation(false);
    }

    void setRotation(bool rotate)
    {
      rotate = false;

      if(rotate)
      {
        buffer[0] = 0x7f;
        buffer[1] = 0xff;
        buffer[2] = 0xff;
      }
      else
      {
        buffer[0] = 0x7f;
        buffer[1] = 0xff;  
        buffer[2] = 0xff;
      }
    }

  };

  class LetterSyncWriter : public CircularByteArrayWriter{

  public: 
    LetterSyncWriter() : CircularByteArrayWriter(20){
      buffer[0] = 0xff;
      buffer[1] = 0x5f;
      buffer[2] = 0xff;
    }
  };

  class LetterFillerWriter : public CircularByteArrayWriter{

  public: 
    LetterFillerWriter(uint8_t address=89, uint8_t position=0){
      buffer[0] = address ^ 255;
      buffer[1] = position ^ 255; 
    }
  };

  //Stala liczba zeby nie nadwyrezac alokacji pamieci
  LetterArrayWriter writers[BOARDCOUNT];
  LetterFillerWriter fillers[BOARDCOUNT];
  LetterSyncWriter sync[BOARDCOUNT];
  RotationWriter rotation;

  unsigned int writersCount;
  bool hasLetterData; //Cached value of letterDataProcessed - in order to minimize LetterArrayWriter::hasMore calls during inactivity
  bool syncing;

public:
  BoardConfigurator(){
    writersCount = 0;
    hasLetterData = false;
    syncing = false;
  };

  bool hasMore(){
    //Koniec wtedy kiedy jestesmy w trybie rotowania i wyslalismy wszystkie bity
    return hasLetterData && !(syncing && !rotation.hasMore());
  }

  bool letterDataProcessed()
  {
    bool result = true;

    for(unsigned int i = 0; i < writersCount; ++i)
      result = result && !writers[i].hasMore();

    return result;
  }

  bool getNextBit(int row_id){ //-1 = bit rotacji; >0 = dane liter wiersza
    if(row_id == -1)
    {
      bool result = rotation.getNextBit();
      if(!rotation.hasMore()){

        if(!syncing)
        {
          rotation.rewind();
          if(letterDataProcessed())
          {
            //Wyslane dane liter - wyczyscmy tablice
            for(unsigned int i = 0; i < writersCount; ++i)
              writers[i].clear();
            debugs("Syncing...");
            syncing = true;
            rotation.setRotation(true);
          }
        }
      }
      return result;
    }
    else
    {
      if(syncing)
        return sync[row_id].getNextBit();

      if(writers[row_id].hasMore())  
        return writers[row_id].getNextBit(); 
      else
        return fillers[row_id].getNextBit();
    }
  }

  void rewind(){
    for(unsigned int i = 0; i < writersCount; ++i)
    {
      writers[i].rewind();
      fillers[i].rewind();
      sync[i].rewind();
    }

    syncing = false;
    rotation.setRotation(false);
    rotation.rewind();
    

    hasLetterData = !letterDataProcessed();
  }

  void addArray(LetterArray* array){
    writers[writersCount++].setArray(array);
  }
};
