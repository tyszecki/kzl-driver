#pragma once

#ifdef ARDUINO
#ifdef DEBUG
#define debug(x) Serial.print(x)
#define debugs(x) Serial.print(x)
#define debugnl(x) Serial.print(x); Serial.print("\n")
#else
#define debug(x) ((void)0)
#define debugs(x) ((void)0)
#define debugnl(x) ((void)0)
#endif
#else
#ifdef DEBUG
#define debug(x) std::cout << std::to_string(x)
#define debugs(x) std::cout << x << "\n"
#define debugnl(x) std::cout << std::to_string(x) << "\n"
#else
#define debug(x) ((void)0)
#define debugs(x) ((void)0)
#define debugnl(x) ((void)0)
#endif
#endif
