#pragma once

#ifndef ARDUINO
#define bitRead(value, bit) (((value) >> (bit)) & 0x01)
#endif

#include <stdint.h>

class ByteArrayWriter{
protected:
  uint8_t *bytePtr;
  uint8_t *endPtr;
  int8_t bitIdx;
  uint8_t buffer[3]; 

  ByteArrayWriter(){
    bitIdx = 7;
    bytePtr = buffer;
    endPtr = &buffer[3];
  }

public:

  bool getNextBit()
  {
    bool result = bitRead(*bytePtr, this->bitIdx--);

    if(this->bitIdx < 0){
      this->bitIdx = 7;
      bytePtr++;
    }

    return result;
  } 

  bool hasMore()
  {
    return bytePtr < endPtr;
  }

  void rewind()
  {
    this->bitIdx = 7;
    bytePtr = buffer;
  }
};