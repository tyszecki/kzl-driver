#ifndef ARDUINO

#define DEBUG
#define BOARDCOUNT 5
#define ARRAY_SIZE 89

#include <string>
#include <iostream>

#include "boardconfigurator.h"



void empty()
{
	LetterArray arrays[5];
	BoardConfigurator config;

	std::string output[6];

	for(int i = 0; i < 5; ++i)
		config.addArray(&arrays[i]);

	while(config.hasMore()){
		output[1] += config.getNextBit(0) ? 1 : 0;

	    output[2] += config.getNextBit(1) ? 1 : 0;
	    output[3] += config.getNextBit(2) ? 1 : 0;
	    
	    output[4] += config.getNextBit(4) ? 1 : 0;
	    output[5] += config.getNextBit(3) ? 1 : 0;

	    //Rot_buf
	    output[0] += config.getNextBit(-1) ? 1 : 0;
	}


	for(int i = 0; i < 5; ++i)
		std::cout << output[i] << "\n";
}

int main()
{
	LetterArray arrays[5];
	BoardConfigurator config;

	std::string output[6];

	for(int i = 0; i < 5; ++i)
		config.addArray(&arrays[i]);

	arrays[0].setLetter(6, 1);
	arrays[0].setLetter(7, 2);
	arrays[0].setLetter(8, 3);
	arrays[0].setLetter(9, 4);
	arrays[0].setLetter(10, 5);
	arrays[0].setLetter(11, 6);
	arrays[0].setLetter(12, 7);
	arrays[0].setLetter(13, 8);
	arrays[0].setLetter(14, 9);
	arrays[0].setLetter(15, 10);
	arrays[0].setLetter(16, 11);
	//arrays[0].setLetter(80, 15);
	//arrays[1].setLetter(5,5);
	config.rewind();

	while(config.hasMore()){
		output[1] += config.getNextBit(0) ? "1," : "0,";

	    output[2] += config.getNextBit(1) ? "1" : "0";
	    output[3] += config.getNextBit(2) ? "1" : "0";
	    
	    output[4] += config.getNextBit(4) ? "1" : "0";
	    output[5] += config.getNextBit(3) ? "1" : "0";

	    //Rot_buf
	    output[0] += config.getNextBit(-1) ? "1," : "0,";
	}


	for(int i = 0; i < 5; ++i)
		std::cout << output[i] << "\n";

}


#endif