#pragma once

#include "bytearraywriter.h"

class PaddedByteArrayWriter : public ByteArrayWriter{
  unsigned int size = 20;
  unsigned int done;
  bool flat;
public:
  PaddedByteArrayWriter(unsigned int _size = 20) : size(_size)
  { 
    done = 0;
  }

  bool getNextBit()
  {
    ++done;

    if(ByteArrayWriter::hasMore())
      return ByteArrayWriter::getNextBit();
    
    return true;
  }

  bool hasMore()
  {
    return (done < size);
  }

  void resize(unsigned int _size)
  {
    size = _size;
  }

  void rewind()
  {
    ByteArrayWriter::rewind();
    done = 0;
  }
};