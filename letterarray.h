#pragma once

#ifndef ARDUINO
#include <cstddef>
#endif

#include "letter.h"

class LetterArray{
  Letter array[ARRAY_SIZE];
  unsigned int dirtyCnt;
public:
  LetterArray(){
    for(int i = 0; i < ARRAY_SIZE; ++i)
    {
      array[i].position = 0;
      array[i].dirty = false;
    }
  }

  //Uzywamy adresu jako pozycji w tabeli, aby wciaz umozliwic wyslanie polecenia na adres 0
  void setLetter(uint8_t address, uint8_t position, bool force_dirty=false)
  {
    if(array[address].position != position || force_dirty)
    {
      array[address].position = position;
      array[address].dirty = true;
    }
  }

  const Letter* getNextDirtyLetter(const Letter* prev)
  {
    for(const Letter *i = prev == NULL ? &array[0] : prev+1; i < &array[ARRAY_SIZE]; ++i)
    {
      if(i->dirty)
        return i;
    }
    return NULL;
  }

  uint8_t getLetterIndex(const Letter* letter)
  {
    return (letter - array);
  }

  void clear()
  {
    for(int i = 0; i < ARRAY_SIZE; ++i)
      array[i].dirty = false;

    dirtyCnt = 0;
  }

};