#pragma once

struct Letter{
  bool dirty : 1;
  uint8_t position : 6;

  Letter(uint8_t _position = 0) : position(_position){
  }
};