#pragma once

#include "letterarray.h"
#include "debug.h"

class LetterArrayWriter : public PaddedByteArrayWriter{
  LetterArray* array;
  const Letter* currentLetter;
  
public:

  LetterArrayWriter(LetterArray* _array = 0) : PaddedByteArrayWriter(){
    array = _array;
    if(array != 0)
    {
      currentLetter = array->getNextDirtyLetter(NULL);
      parseLetter();
    }
  }

  void setArray(LetterArray* _array){
    array = _array;
    currentLetter = array->getNextDirtyLetter(NULL);
  }

  bool getNextBit()
  {
    bool result = PaddedByteArrayWriter::getNextBit();

    if(!PaddedByteArrayWriter::hasMore())
    {
      PaddedByteArrayWriter::rewind(); 
      currentLetter = array->getNextDirtyLetter(currentLetter);
      parseLetter();
    }

    return result;
  }

  bool hasMore()
  {
    return PaddedByteArrayWriter::hasMore() && currentLetter != NULL;
  }

  void rewind()
  {
    PaddedByteArrayWriter::rewind();
    currentLetter = array->getNextDirtyLetter(NULL);
    parseLetter();
  }

  void clear()
  {
    array->clear();
  }

  void parseLetter()
  {
    if(currentLetter == NULL)
      return;

    debugnl(array->getLetterIndex(currentLetter));

    buffer[0] = array->getLetterIndex(currentLetter) ^ 255;

    uint8_t position = currentLetter->position;


    //buffer[1] = result; 
    buffer[1] = ((position >> 2) | 0x10) ^ 255;
    //buffer[2] = ((position << 6) ^ 255) & 0xCF;  //Czy to 0xCF jest potrzebne?
    buffer[2] = ((position << 6) ^ 255); 

  }
};