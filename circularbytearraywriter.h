#pragma once

class CircularByteArrayWriter : public PaddedByteArrayWriter{
public:
  CircularByteArrayWriter(unsigned int size = 20) : PaddedByteArrayWriter(size){}
  
  bool getNextBit(){
    bool result = PaddedByteArrayWriter::getNextBit();

    if(!PaddedByteArrayWriter::hasMore())
      PaddedByteArrayWriter::rewind();

    return result;
  }
  bool hasMore(){
    return true;
  }
};
